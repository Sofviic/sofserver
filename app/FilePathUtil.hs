module FilePathUtil where

import qualified Data.ByteString.Char8 as C
import qualified Data.ByteString.Lazy.Char8 as L
import System.Directory

import Types
import Utils

pathIn :: [FilePath] -> [FilePath] -> Bool
pathIn a b = bb `prefixes` aa
                where
                        aa = fmap (takeWhile (/= '/') . dropWhile (== '/')) a
                        bb = fmap (takeWhile (/= '/') . dropWhile (== '/')) b

concatFilePath :: [FilePath] -> FilePath
concatFilePath = concatWith "/"
