{-# LANGUAGE OverloadedStrings #-}
module Config (
    portNumber,
    configVersion,
    serverVersion
    ) where

import Types

portNumber :: Int
portNumber = 8136

configVersion :: Content
configVersion = "v0.2.0.0"

serverVersion :: Content
serverVersion = "v0.3.3.5"
