{-# LANGUAGE OverloadedStrings #-}
module ConfigFile where
    
import qualified Data.ByteString.Char8 as C
import qualified Data.ByteString.Lazy.Char8 as L
import System.Directory

import Types
import Utils
import FilePathUtil

page :: [FilePath] -> IO (ContentSort, Content)
page uri = do
                conf <- readFile "src/files.sof.conf"
                (cs,c) <- parseFilesConf (lines conf) uri
                sequence (cs, L.readFile c)

parseExposeConf :: [String] -> [FilePath] -> IO (Maybe (ContentSort,FilePath))
parseExposeConf conf uri = let fexp = (!! 1) . words . head . filter ("expose=" `prefixes`) $ conf
                           in do exists <- doesFileExist $ concatFilePath uri
                                 if exists && uri `pathIn` [fexp]
                                        then do putStrLn "======================="
                                                putStrLn $ concatFilePath uri
                                                putStrLn . show $ exists
                                                putStrLn "======================="
                                                return . Just $ ("text/plain", concatFilePath uri)
                                        else return $ Nothing

parseFilesConf :: [String] -> [FilePath] -> IO (ContentSort,FilePath)
parseFilesConf conf uri = do pec <- parseExposeConf conf uri
                             case pec of
                                Just res -> return res
                                Nothing -> if length (pages uri) < 1
                                                then return . fmap (fdir++) $ ferr
                                                else return . fmap (fdir++) . head . pages $ uri
                                where
                                        fdir = (!! 1) . words . head . filter ("dir=" `prefixes`) $ conf
                                        fdef = readFilesConfLine . head . filter ("!def" `prefixes`) $ conf
                                        ferr = readFilesConfLine . head . filter ("!err" `prefixes`) $ conf
                                        pages = fileMappings 
                                                $ preappendDefFileConf fdef
                                                $ parseFilesConfPages conf

fileMappings :: [[FilePath] -> Maybe (ContentSort,FilePath)] -> [FilePath] -> [(ContentSort,FilePath)]
fileMappings = collapseMaybeFuncs

preappendDefFileConf :: (ContentSort,FilePath) 
                        -> [[FilePath] -> Maybe (ContentSort,FilePath)] 
                        -> [[FilePath] -> Maybe (ContentSort,FilePath)]
preappendDefFileConf def = ([deff] ++)
                                where
                                        deff [] = Just def
                                        deff _  = Nothing

parseFilesConfPages :: [String] -> [[FilePath] -> Maybe (ContentSort,FilePath)]
parseFilesConfPages = fmap parseFilesConfLine . filter validConfPageLine

validConfPageLine :: String -> Bool
validConfPageLine s = case words s of
                        [_, ":", _, ",", _] -> True
                        otherwise           -> False

readFilesConfLine :: String -> (ContentSort,FilePath)
readFilesConfLine s = (C.pack cs,fp)
                        where [_, ":", cs, ",", fp] = words s

parseFilesConfLine :: String -> [FilePath] -> Maybe (ContentSort,FilePath)
parseFilesConfLine s path = if uri == path
                                then Just (C.pack cs,fp)
                                else Nothing
                                where 
                                        [uristr, ":", cs, ",", fp] = words s
                                        uri = splitStringCleanOn "/" uristr


