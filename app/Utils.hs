module Utils where

concatWith :: [a] -> [[a]] -> [a]
concatWith _ []     = []
concatWith _ [s]    = s
concatWith w (s:ss) = s ++ w ++ concatWith w ss

prefixes :: Eq a => [a] -> [a] -> Bool
prefixes [] _          = True
prefixes _  []         = False
prefixes (a:as) (b:bs) = if a /= b
                                then False
                                else prefixes as bs

trimWhiteSpace :: String -> String
trimWhiteSpace = dropWhile isWhiteSpace . reverse . dropWhile isWhiteSpace . reverse

isWhiteSpace :: Char -> Bool
isWhiteSpace ' '  = True
isWhiteSpace '\t' = True
isWhiteSpace '\r' = True
isWhiteSpace '\n' = True
isWhiteSpace _    = False

splitStringCleanOn :: String -> String -> [String]
splitStringCleanOn = fmap trimWhiteSpace .#. splitCleanOn

splitCleanOn :: Eq a => [a] -> [a] -> [[a]]
splitCleanOn = filter (/= []) .#. splitCleanOn'

splitCleanOn' :: Eq a => [a] -> [a] -> [[a]]
splitCleanOn' _  []   = []
splitCleanOn' ds x    = 
                        [takeWhile (not . (`elem` ds)) x] ++
                        splitCleanOn' ds (drop 1 $ dropWhile (not . (`elem` ds)) x)

infixr 9 .#.
(.#.) :: (c -> d) -> (a -> b -> c) -> a -> b -> d
f .#. g = (f .) . g

dropLast :: Int -> [a] -> [a]
dropLast n = reverse . drop n . reverse

collapseMaybeFuncs :: [a -> Maybe b] -> a -> [b]
collapseMaybeFuncs fs a = catMaybes $ fmap ($ a) fs

catMaybes :: [Maybe a] -> [a] 
catMaybes []            = []
catMaybes (Just x:xs)   = x : catMaybes xs
catMaybes (Nothing:xs)  = catMaybes xs
