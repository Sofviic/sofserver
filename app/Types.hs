{-# LANGUAGE DeriveGeneric #-}
module Types where

import qualified Data.ByteString.Char8 as C
import qualified Data.ByteString.Lazy.Char8 as L
import GHC.Generics

type Content = L.ByteString
type ContentSort = C.ByteString

-- TODO: Use the following `data`s
data File = File {
          filename :: FilePath
        , contents :: Content
        } deriving (Generic, Show)

data Command = Command {
          command :: String
        , args    :: [Content]
        } deriving (Generic, Show)

