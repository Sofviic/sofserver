{-# LANGUAGE OverloadedStrings #-}
module Main (main) where

import Control.Monad.IO.Class
import qualified Control.Concurrent.MVar as MV
import qualified Data.ByteString.Lazy.Char8 as L
import System.Directory
import Happstack.Server

import Config
import ConfigFile
import Types

import Utils

-- TODO: Use Yaml/JSON in request/response

config :: Conf
config = Conf portNumber Nothing (Just logMAccess) 30 Nothing

main :: IO ()
main = simpleHTTP config $ app

app :: ServerPartT IO Response
app = do
        Request{rqMethod = method,
                rqPaths = p,
                rqInputsQuery = qs,
                rqBody = mvbody
                } <- askRq
        (Body body) <- liftIO $ MV.readMVar mvbody
        (ct, content) <- liftIO $ page p
        if method == POST
          then case (trimWhiteSpace . cmdPost) body of
                "debug" -> ok . toResponseBS "text/plain" $ body
                "upload" -> do
                                let (fp, c) = parsePOSTUpload body
                                liftIO $ L.putStrLn body
                                liftIO $ L.writeFile ("data/" ++ fp) c
                                resp 201 . toResponseBS "text/plain" $ c
                "delete" -> do
                                let fp = bodySinglePost body
                                liftIO $ L.putStrLn body
                                liftIO $ removeFile $ trimWhiteSpace $ L.unpack ("data/" `L.append` fp)
                                resp 201 . toResponseBS "text/plain" $ "removed file " `L.append` fp
                "getfiles" -> do
                                files <- liftIO $ listDirectory "data/"
                                ok . toResponseBS "text/plain" . L.pack . unlines $ files
                "version-server" -> ok . toResponseBS "text/plain" $ serverVersion
                "varsion-config" -> ok . toResponseBS "text/plain" $ configVersion
                _ -> ok . toResponseBS "text/plain" $ body `L.append` (L.pack . cmdPost) body
          else ok . toResponseBS ct $ debugInfo qs `L.append` content

cmdPost :: Content -> String
cmdPost = L.unpack . (!! 3) . L.lines

bodySinglePost :: Content -> Content
bodySinglePost = (!! 7) . L.lines

bodyPost :: Content -> Content
bodyPost = L.unlines . dropLast 1 . drop 7 . L.lines

parsePOSTUpload :: Content -> (FilePath,Content)
parsePOSTUpload body = (filename body, bodyPost body)
                        where
                                filename = L.unpack
                                        . (!! 1) . L.split '"'
                                        . (!! 2) . L.split ';'
                                        . (!! 5) . L.lines

debugInfo :: [(String, Input)] -> Content
debugInfo qs = if ("debug","true") `elem` (fmap.fmap) show qs
                then " = DEBUG IS ON = \n"
                else ""





