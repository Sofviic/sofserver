# sofserver

This is a custom-made webserver in **haskell/happstack** that I'm running on my **rpi4**.  
**Requires** a `src/files.sof.conf` file.

Server Port: **8136**  
Server Version: **v0.3.3**  
Config Version: **v0.2.0**  

Released under **BSD-3**.  
(**Note**: This does **NOT** include files under the `data/` directory.)

## Usage

Change the hardcoded **IP** `192.168.1.7` in:
- src/fileDownload.html
- src/fileDownload.html
- src/fileUpload.html

Optional: change the hardcoded **Port** `8136` in:
- app/Config.hs*
- src/fileDownload.html
- src/fileDownload.html
- src/fileUpload.html

*: Requires rebuilding (`stack build` in the directory).
